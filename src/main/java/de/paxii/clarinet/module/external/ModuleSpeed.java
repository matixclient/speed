package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.module.killaura.TimeManager;
import de.paxii.clarinet.util.module.settings.ValueBase;

/**
 * Created by Lars on 06.02.2016.
 */
public class ModuleSpeed extends Module {
  private TimeManager timeManager;

  public ModuleSpeed() {
    super("Speed", ModuleCategory.MOVEMENT);

    this.setVersion("1.0");
    this.setBuildVersion(15801);

    this.timeManager = new TimeManager();
    this.getModuleValues().put("maxSpeed", new ValueBase("Speed", 1.5F, 1.0F, 5.0F, false));
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent event) {
    this.timeManager.updateTimer();

    if (Wrapper.getPlayer().isSneaking() || Wrapper.getPlayer().moveForward == 0
            && Wrapper.getPlayer().moveStrafing == 0) {
      return;
    }

    double maxSpeed = this.getModuleValues().get("maxSpeed").getValue();

    if (Wrapper.getPlayer().moveForward > 0 && !Wrapper.getPlayer().isCollidedHorizontally) {
      Wrapper.getPlayer().setSprinting(true);
    }

    if (Wrapper.getPlayer().onGround && this.timeManager.sleep(150L)) {
      this.timeManager.updateLast();

      Wrapper.getPlayer().motionX *= maxSpeed;
      Wrapper.getPlayer().motionZ *= maxSpeed;
    }
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
